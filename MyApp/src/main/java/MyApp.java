import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class MyApp {

    public static void main(String args[]){
        if(args.length > 0){
            Argument argument = new Argument(args[0]);
            argument.print_results();
        }
    }
}
