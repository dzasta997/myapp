import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class Argument {

    private String string;
    private SortedMap<Character, Integer> counted_letters = new TreeMap<>();

    public Argument(String string){
        this.string = string;
        this.counted_letters = count_letters(string);
    }

    public SortedMap<Character, Integer> count_letters(String string){
        SortedMap<Character, Integer> counted_letters = new TreeMap();
        for(int i = 0; i<string.length(); i++){
            Character ch = Character.toLowerCase(string.charAt(i));
            int occur = counted_letters.containsKey(ch) ? counted_letters.get(ch) : 0;
            counted_letters.put(ch, occur + 1);
        }
        return counted_letters;
    }

    public void print_results() {
        System.out.println("Counted letters:");
        for (Map.Entry<Character,Integer> entry : this.counted_letters.entrySet()){
            System.out.println(entry.getKey()+" = "+entry.getValue());
        }
    }

}
