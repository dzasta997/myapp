# MyApp



## Counting Letters

This is a simple application for counting letter occurrences in a string.
## Description

Application takes string as an argument, counts occurrences of each letter and displays the result in alphabetical order.


## Note

Based on a given task description following assumptions were made:

- [ ] Letters are counted case-insensitive
- [ ] Not only letters but all chars are allowed
